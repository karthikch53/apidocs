package com.rest.sample;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;

import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;

@Path("/sample")
@Api(value = "/sample", description = "Say hello to user")
public class SampleResource {
	
	@GET
	@Path("/{name}")
	@ApiOperation(value = "Find pet by ID", notes = "Add extra notes here")
	public Response sayHelloToUser(@PathParam("name") String name) 
	{
		String output = "Hello Mr. " + name + ". Welcome..!";
		return Response.status(200).entity(output).build();
	}

}
